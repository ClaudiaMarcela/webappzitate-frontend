# Web application Zitate (frontend)

_Web application for booking appointments in different commercial establishments of a city_



## Developed with 🛠️

_The frontend was developed with TypeScript/HTML/CSS with an object-oriented programming approach, and designed as a single page aplication. The following tools were used:_
* Visual Studio Code - IDE for code writing
* Angular - SPA framework



## Deployment 🚀

_The following platforms were used for deploying the application:_
* Database --> Clever cloud
* Backend --> Heroku
* Frontend --> Heroku

The web application is available in the URL: https://webzitate.herokuapp.com/

Note: website loading could be slow because of free servers



## Project date 📌

2022 - in progress
