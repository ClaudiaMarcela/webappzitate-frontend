import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { LoginComponent } from './components/login/login.component';
import { MostrarNegociosComponent } from './components/mostrar-negocios/mostrar-negocios.component';
import { MostrarServiciosComponent } from './components/mostrar-servicios/mostrar-servicios.component';
import { RegistroComponent } from './components/registro/registro.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'mostrar-negocios/:categoria', component: MostrarNegociosComponent}, //Path dinamico
  {path: 'mostrar-servicios', component: MostrarServiciosComponent},
  {path: '', component: InicioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
