//ESTRUCTURA DE DATOS DE UN NEGOCIO
export interface NegocioModel{
    id_negocio: number,
    nombre_negocio: string,
    descripcion_negocio: string,
    direccion_negocio: string,
    telefono_negocio: string,
    email_negocio: string,
    status_negocio: string,
    categoria_negocio: string,
    id_administrador: number,
    password_negocio: string
}