import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderSimpleComponent } from './components/header-simple/header-simple.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { UsuariosService } from './services/usuarios/usuarios.service';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MostrarNegociosComponent } from './components/mostrar-negocios/mostrar-negocios.component';
import { MostrarServiciosComponent } from './components/mostrar-servicios/mostrar-servicios.component';
import { HeaderComponent } from './components/header/header.component';
import { NegociosService } from './services/negocios/negocios.service';
import { ServiciosService } from './services/servicios/servicios.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderSimpleComponent,
    FooterComponent,
    LoginComponent,
    RegistroComponent,
    InicioComponent,
    MostrarNegociosComponent,
    MostrarServiciosComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, //Importar modulo de http Client
    ReactiveFormsModule
  ],
  providers: [UsuariosService, NegociosService, ServiciosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
