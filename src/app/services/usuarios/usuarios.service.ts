import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) { } //Inyectar dependencia http Client 


  /*PETICIONES HTTP
  Se deben enviar los parametros ,tal como lo espera el backend
  */

  public obtenerUsuario(){

  }
  
  public agregarUsuario(usuario: any): Promise<any>{
    const url = `${environment.apiUrl}/agregarUsuario`; //ruta backend
    return this.http.post(url, usuario).toPromise(); //Enviar datos como body
  }

  public actualizarUsuario(){

  }

  public desactivarUsuario(){

  }

  public validarUsuario(datos: any): Promise<any>{
    const url = `${environment.apiUrl}/validarUsuario`; //ruta backend
    return this.http.post(url, datos).toPromise(); //Enviar datos como body
  }
  
  




}
