import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NegociosService {

  constructor(private http: HttpClient) { }


/*PETICIONES HTTP
  Se deben enviar los parametros ,tal como lo espera el backend
  */

  public obtenerNegocios(categoria: string): Promise<any>{
    const url = `${environment.apiUrl}/obtenerNegocios/${categoria}`; //ruta backend dinamica
    return this.http.get(url).toPromise();
  }
  
}
