import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor(private http: HttpClient) { }
}


/*PETICIONES HTTP
  Se deben enviar los parametros ,tal como lo espera el backend
  */
  /*
  public obtenerServicios(){

  }
  
  public agregarUsuario(usuario: any): Promise<any>{
    const url = `${environment.apiUrl}/agregarUsuario`; //ruta backend
    return this.http.post(url, usuario).toPromise(); //Enviar datos como body
  }
  */
