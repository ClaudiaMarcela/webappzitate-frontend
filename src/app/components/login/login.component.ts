import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //Atributos
  public formaUsuarios: FormGroup = new FormGroup({}); //Para guardar el conjunto de inputs del formulario registro


  //Constructor
  constructor(private usuariosService: UsuariosService, private formBuilder: FormBuilder, private router: Router, private location: Location) { }


  //Metodo al iniciar componente
  ngOnInit(): void {
    this.buildUsuarios(); //Construir grupo de inputs
  }


  //Metodo para crear grupo de inputs del formulario registro
  private buildUsuarios(){
    this.formaUsuarios = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.maxLength(10)]]
    });
  }


  //Metodo para validar usuario: llamar peticion HTTP
  public validarUsuario(){
    this.usuariosService.validarUsuario(this.formaUsuarios.value).then(response =>{
      //Backend retorna: correct, info / incorrect / not found
      if(response.message == 'correct'){
        sessionStorage.setItem('id', (response.user.id_usuario).toString()); //Guardar info en sessionStorage
        sessionStorage.setItem('nombre', response.user.nombre_usuario);
        sessionStorage.setItem('token', response.user.token)
        //this.router.navigate(['/']);
        this.goBack();
      }else{
        alert('Correo electrónico o contraseña incorrecto(s)');
        this.buildUsuarios(); //Reinicia formulario
      }
    }).catch(error =>{
      console.log(error);
    })
  }


  goBack(){
    this.location.back();
  }

}
