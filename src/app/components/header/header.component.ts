import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public token: string | null = null;
  public nombre: string | null = null;

  constructor() { }

  ngOnInit(): void {
    const valor = sessionStorage.getItem('token');
    this.token = valor ? valor : null;
    this.nombre = sessionStorage.getItem('nombre');
  }

  public cerrarSesion(){
    sessionStorage.clear();
    this.ngOnInit();
  }

}
