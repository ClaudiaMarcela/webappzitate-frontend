import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NegocioModel } from 'src/app/models/negocio';
import { NegociosService } from 'src/app/services/negocios/negocios.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mostrar-negocios',
  templateUrl: './mostrar-negocios.component.html',
  styleUrls: ['./mostrar-negocios.component.scss']
})
export class MostrarNegociosComponent implements OnInit {

  //Atributo
  public negocios: NegocioModel[] = []; //Para guardar datos traidos de la BD
  public catego: string = 'categoria';

  //Inyectar dependencias
  constructor(private negociosService: NegociosService, private router: Router, private route: ActivatedRoute, private location: Location) { }

  async ngOnInit(): Promise<void> {
    
    //Obtener el parámetro enviado por la URL del componente
    this.route.params.subscribe(async params => {
      const categoria: string = params['categoria']
      //console.log(categoria, params);
      this.negocios = await this.obtenerNegocios(categoria); //Le paso la variable
      this.catego = categoria;
    });    
  }

  //Metodo para obtener negocios: llamar peticion HTTP
  public async obtenerNegocios(categoria: string): Promise<any>{
    try{
      const response = await this.negociosService.obtenerNegocios(categoria);     
      //backend devuelve un mensaje y los registros de la BD
      return response.datos; //Retorna array, en cada posicion un json que contiene un registro de la BD

    }catch(error){
      console.log(error);
    }
  }

  goBack(){
    this.location.back();
  }


}
