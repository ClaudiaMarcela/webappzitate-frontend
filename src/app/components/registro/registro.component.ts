import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  //Atributos
  public formUsuarios: FormGroup = new FormGroup({}); //Para guardar el conjunto de inputs del formulario registro


  //Constructor
  constructor(private usuariosService: UsuariosService, private formBuilder: FormBuilder, private router: Router, private location: Location) { }


  //Metodo al iniciar componente
  ngOnInit(): void {
    this.buildUsuarios(); //Construir grupo de inputs
  }


  //Metodo para crear grupo de inputs del formulario registro
  private buildUsuarios(){
    this.formUsuarios = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.maxLength(30)]],
      apellido: ['', [Validators.required, Validators.maxLength(30)]],
      telefono: ['', [Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
      status: ['Activo'],
      password: ['', [Validators.required, Validators.maxLength(10)]],
      identificacion: ['', [Validators.required, Validators.maxLength(10)]],
      tipo: ['', Validators.required],
      recuperar: ['0'],
      rol: ['Cliente']
    });
  }

  //Metodo para agregar usuario: llamar peticion HTTP
  public agregarUsuario(){
    this.usuariosService.agregarUsuario(this.formUsuarios.value).then(response =>{
      //Backend retorna mensaje todo ok y el id
      if(response.message == 'created'){
        alert('Usuario registrado correctamente');
        this.goBack();
        //this.router.navigate(['/login']); //Redirecciona a la vista de login
      }else{
        alert('Ya existe un usuario registrado con este correo');
        this.buildUsuarios(); //Limpia formulario
      }
    }).catch(error =>{
      console.log(error);
    })
  }

  
  goBack(){
    this.location.back();
  }

  get nombre() {
    return this.formUsuarios.get('nombre');
  }
  
}
